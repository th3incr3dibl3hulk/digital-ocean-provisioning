import os
#requires pre-installation and configuration of Digital Ocean CLI tool

#pick the instance name
name_of_instance = input("What should the new instance be named? ")

#pick the instance size
os.system('doctl compute size list')
size_of_instance = input("What size should the new instance be? ")

os.system('doctl compute image list-distribution')
instance_image = input("What image should be used? ")


#build the instance
os.system('doctl compute droplet create {0} --size {1} --image {2} --region nyc1 --ssh-keys 23432941'.format(name_of_instance, size_of_instance, instance_image))
